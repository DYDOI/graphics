window.onload = function () {
    const graph = document.createElement('canvas');
    const wrapper = document.getElementById('wrapper');
    const inputs = document.createElement('div');
    inputs.id = 'inputs';
    wrapper.appendChild(graph);
    wrapper.appendChild(inputs);

    // Рендерим инпуты для создания линии
    const x1cda = document.createElement('input');
    x1cda.placeholder = "x1";
    const y1cda = document.createElement('input');
    y1cda.placeholder = "y1";
    const x2cda = document.createElement('input');
    x2cda.placeholder = "x2";
    const y2cda = document.createElement('input');
    y2cda.placeholder = "y2";
    const submit = document.createElement('div');
    submit.className = "btn btn-success";
    submit.innerHTML = "submit cda";

    const x1brez = document.createElement('input');
    x1brez.placeholder = "x1";
    const y1brez = document.createElement('input');
    y1brez.placeholder = "y1";
    const x2brez = document.createElement('input');
    x2brez.placeholder = "x2";
    const y2brez = document.createElement('input');
    y2brez.placeholder = "y2";
    const brez = document.createElement('div');
    brez.className = "btn btn-warning";
    brez.innerHTML = "submit brez";

    inputs.appendChild(x1cda);
    inputs.appendChild(y1cda);
    inputs.appendChild(x2cda);
    inputs.appendChild(y2cda);
    inputs.appendChild(submit);

    inputs.appendChild(x1brez);
    inputs.appendChild(y1brez);
    inputs.appendChild(x2brez);
    inputs.appendChild(y2brez);
    inputs.appendChild(brez);


    // Задаем тип графики
    let ctx = graph.getContext('2d');
    ctx.translate(0, graph.height);
    ctx.rotate(-Math.PI/2);
    // Задаем размеры холста
    ctx.canvas.width  = 600;
    ctx.canvas.height = 600;

    // ЦДА
    submit.addEventListener("click",() => {

    //Интерпритация координат canvas в декартовы
    let x1can = parseInt(x1cda.value);
    let x2can = parseInt(x2cda.value);
    let y1can = parseInt(y1cda.value);
    let y2can = parseInt(y2cda.value);
    let x1,x2,y1,y2;
    x1 = 0;
    x2 = 0;
    y1 = 0;
    y2 = 0;

    if (x1can < 0){
        let i = 0;
        let count = 12 + x1can;
        while (i !== count) {
            x1 = x1 + 25;
            i++;
        }
        // console.log(x1can,x2can,y1can,y2can);
        // console.log(x1,i);
    } else {
        let i = 0;
        let count = 12 + x1can;
        while (i !== count) {
            x1 = x1 + 25;
            i++;
        }
    }

    if (x2can < 0){
        let i = 0;
        let count = 12 + x2can;
        while (i !== count) {
            x2 = x2 + 25;
            i++;
            // console.log(x2,i);
        }
            // console.log(x1can,x2can,y1can,y2can);
            // console.log(x1,i);
    } else {
        let i = 0;
        let count = 12 + x2can;
        while (i !== count) {
            x2 = x2 + 25;
            i++;
            // console.log(x1,i);
        }
    }

    if (y1can < 0){
            let i = 0;
            let count = 12 + Math.abs(y1can);
            while (i !== count) {
                y1 = y1 + 25;
                i++;

            }
        } else {
            let i = 0;
            let count = 12 + (y1can - (y1can * 2));
            while (i !== count) {
                y1 = y1 + 25;
                i++;
                // console.log(x1,i);
            }
        }

    if (y2can < 0){
            let i = 0;
            let count = 12 + Math.abs(y2can);
            while (i !== count) {
                y2 = y2 + 25;
                i++;

            }
        } else {
            let i = 0;
            let count = 12 + (y2can - (y2can * 2));
            while (i !== count) {
                y2 = y2 + 25;
                i++;
                // console.log(x1,i);
            }
        }

    // console.log(x1,y1,x2,y2);


    let len;

    if (Math.abs(x2 - x1) >= Math.abs(y2 - y1)){
        len = Math.abs(x2 - x1);
    } else {
        len = Math.abs(y2 - y1);
    }

    let xIncriment = (x2 - x1) / len;
    let yIncriment = (y2 - y1) / len;
    let x = x1 + Math.sign(xIncriment);
    let y = y1 + Math.sign(yIncriment);
    let i = 0;

    // Построение по ЦДА для 45 градусных прямых
    if (Math.abs(x1) === Math.abs(y1) && Math.abs(x2) === Math.abs(y2)) {
        x1 = Math.abs(x1);
        y1 = Math.abs(y1);
        x2 = Math.abs(x2);
        y2 = Math.abs(y2);
        console.log(x1,y1,x2,y2);

        console.log("first if");
        ctx.strokeStyle = "black";
        ctx.fillStyle = "red";
        ctx.beginPath();
        // Рисуем квадрат ,если прямая проходит через пересечение сеток
        if (y1 % 25 === 0 && x1 % 2 === 0) {
            ctx.moveTo(x1, y1);
            ctx.fillRect(x1, y1, 25, 25);
        }

        // Рисуем квадрат,если линия  переходит пересечение сетки,но не начинает с пересечения
        let crossX = x1;
        let crossY = y1;
        if(y1 % 25 !== 0 && x1 % 25 !== 0) {
            while(crossY % 25 !== 0 && crossX % 25 !== 0){
                crossX = crossX - 1;
                crossY = crossY - 1;
            }
            console.log(crossX,crossY);
            ctx.fillRect(crossX, crossY, 25, 25);
        }

        while (i <= len) {
            ctx.lineTo(x, y);
            x = x + xIncriment;
            y = y + yIncriment;
            if (y % 25 === 0) {
                ctx.fillRect(x, y, 25, 25);
            }
            i++;
        }
        ctx.fillStyle = "red";
        ctx.fill();
        ctx.stroke();
    }

    if(Math.abs(x1) !== Math.abs(y1) || Math.abs(x2) !== Math.abs(y2)){
        console.log("second if");
        ctx.strokeStyle = "black";
        ctx.fillStyle = "red";
        ctx.beginPath();
        let crossX = x1;
        let crossY = y1;
            // Цикл для закраски первого пикселя
            while (true) {
                if (crossX % 25 !== 0) {
                    crossX = crossX - 1;
                }
                if (crossY % 25 !== 0) {
                    crossY = crossY - 1;
                }
                if (crossX % 25 === 0 && crossY % 25 === 0){
                    break;
                }
            }
            console.log("start:" + crossX, crossY);
            ctx.fillStyle = "green";
            ctx.fillRect(crossX, crossY, 25, 25);


            // Цикл для закраски середины прямой
            while (i <= len) {
                ctx.lineTo(x, y);
                x = x + xIncriment;
                y = y + yIncriment;

                if (Math.round(x) % 25 === 0 && Math.round(y) % 25 !== 0){
                    let crossY = Math.round(y);
                    while(crossY % 25 !== 0){
                        crossY = crossY - 1;
                    }
                    console.log("middle:" + x, crossY);
                    ctx.fillStyle = "red";
                    ctx.fillRect(x, crossY, 25, 25);
                }

                i++;
            }

        ctx.fillStyle = "red";
        ctx.fill();
        ctx.stroke();

    }
});
    // БРЕЗ
    brez.addEventListener("click",() => {
        //Интерпритация координат canvas в декартовы

        let x1can = parseInt(x1brez.value);
        let x2can = parseInt(x2brez.value);
        let y1can = parseInt(y1brez.value);
        let y2can = parseInt(y2brez.value);
        let x1,x2,y1,y2;
        x1 = 0;
        x2 = 0;
        y1 = 0;
        y2 = 0;


        if (x1can < 0){
            let i = 0;
            let count = 12 + x1can;
            while (i !== count) {
                x1 = x1 + 25;
                i++;
            }
            // console.log(x1can,x2can,y1can,y2can);
            // console.log(x1,i);
        } else {
            let i = 0;
            let count = 12 + x1can;
            while (i !== count) {
                x1 = x1 + 25;
                i++;
            }
        }

        if (x2can < 0){
            let i = 0;
            let count = 12 + x2can;
            while (i !== count) {
                x2 = x2 + 25;
                i++;
                // console.log(x2,i);
            }
            // console.log(x1can,x2can,y1can,y2can);
            // console.log(x1,i);
        } else {
            let i = 0;
            let count = 12 + x2can;
            while (i !== count) {
                x2 = x2 + 25;
                i++;
                // console.log(x1,i);
            }
        }

        if (y1can < 0){
            let i = 0;
            let count = 12 + Math.abs(y1can);
            while (i !== count) {
                y1 = y1 + 25;
                i++;

            }
        } else {
            let i = 0;
            let count = 12 + (y1can - (y1can * 2));
            while (i !== count) {
                y1 = y1 + 25;
                i++;
                // console.log(x1,i);
            }
        }

        if (y2can < 0){
            let i = 0;
            let count = 12 + Math.abs(y2can);
            while (i !== count) {
                y2 = y2 + 25;
                i++;

            }
        } else {
            let i = 0;
            let count = 12 + (y2can - (y2can * 2));
            while (i !== count) {
                y2 = y2 + 25;
                i++;
                // console.log(x1,i);
            }
        }
        let x = x1;
        let y = y1;
        console.log(x1,y1,x2,y2, x, y);
        let xIncriment = x2 - x1;
        let yIncriment = y2 - y1;
        let e = 2 * yIncriment - xIncriment;
        let i = xIncriment;
        ctx.strokeFill = "black";

        // Построение по БРЕЗ> для 45 градусных прямых
        if (Math.abs(x1) === Math.abs(y1) && Math.abs(x2) === Math.abs(y2)) {

            // Рисуем квадрат,если линия  переходит пересечение сетки
            if (y1 % 25 === 0 && x1 % 25 === 0) {
                ctx.fillStyle = "green";
                ctx.fillRect(x1, y1, 25, 25);
            }
            // Рисуем квадрат,если линия  переходит пересечение сетки,но не начинает с пересечения
            let crossX = x1;
            let crossY = y1;
            if (y1 % 25 !== 0 && x1 % 25 !== 0) {
                while (crossY % 25 !== 0 && crossX % 25 !== 0) {
                    crossX = crossX - 1;
                    crossY = crossY - 1;
                }
                console.log(crossX, crossY);
                ctx.fillStyle = "green";
                ctx.fillRect(crossX, crossY, 25, 25);
            }

            while (i >= 0) {
                if (e >= 0) {
                    x = x + 1;
                    y = y + 1;
                    e = e + 2 * (yIncriment - xIncriment);
                } else {
                    x = x + 1;
                    e = e + 2 * yIncriment;
                }
                if (y % 25 === 0) {
                    ctx.fillRect(x, y, 25, 25);
                }
                ctx.lineTo(x, y);
                i = i - 1;
            }
            console.log("end");
            ctx.stroke();
        }

        // Построение по БРЕЗ для не 45 градусных прямых
        if(Math.abs(x1) !== Math.abs(y1) || Math.abs(x2) !== Math.abs(y2)) {
            console.log("second if");
            ctx.strokeStyle = "black";
            ctx.fillStyle = "red";

            let crossX = x1;
            let crossY = y1;
            // Цикл для закраски первого пикселя
            while (true) {
                if (crossX % 25 !== 0) {
                    crossX = crossX - 1;
                }
                if (crossY % 25 !== 0) {
                    crossY = crossY - 1;
                }
                if (crossX % 25 === 0 && crossY % 25 === 0){
                    break;
                }
            }

            console.log("start:" + crossX, crossY);
            ctx.fillStyle = "green";
            ctx.fillRect(crossX, crossY, 25, 25);

            while (i >= 0) {
                if (e >= 0) {
                    x = x + 1;
                    y = y + 1;
                    e = e + 2 * (yIncriment - xIncriment);
                } else {
                    x = x + 1;
                    e = e + 2 * yIncriment;
                }

                // if (Math.round(x) % 25 === 0 && Math.round(y) % 25 !== 0){
                //     let crossY = Math.round(y);
                //     while(crossY % 25 !== 0){
                //         crossY = crossY - 1;
                //     }
                //     console.log("middle:" + x, crossY);
                //     ctx.fillStyle = "red";
                //     ctx.fillRect(x, crossY, 25, 25);
                // }

                if (Math.round(y) % 25 === 0){
                    let crossX = Math.round(x);
                    while(crossX % 25 !== 0){
                        crossX = crossX - 1;
                    }
                    console.log("middle:" + crossX, y);
                    ctx.fillStyle = "yellow";
                    ctx.fillRect(crossX, y, 25, 25);
                }

                if (Math.round(x) % 25 === 0){
                    let crossY = Math.round(y);
                    while(crossY % 25 !== 0){
                        crossY = crossY - 1;
                    }
                    console.log("middle:" + x, crossY);
                    ctx.fillStyle = "yellow";
                    ctx.fillRect(x, crossY, 25, 25);
                }

                // if (Math.round(x) % 25 === 0 && Math.round(y) % 25 === 0) {
                //     ctx.fillStyle = "red";
                //     ctx.fillRect(x, y, 25, 25);
                // }

                ctx.lineTo(x, y);
                i = i - 1;
            }
            console.log("end");
            ctx.stroke();
        }
    });

// Генерируем сетку холста
    let w = graph.width - 1;
    let h = graph.height - 1;
    let span = 25;

    for (let x = -0.5; x < w; x += span) {
        if (x === 299.5) {
            ctx.strokeStyle= "red";
            ctx.strokeRect(x, 0, 0.1, h);
        }
        ctx.strokeStyle = "rgba(0,0,0,0.4)";
        ctx.strokeRect(x, 0, 0.1, h);
    }

    for (let y = -0.5; y < h; y += span) {
        if (y === 299.5) {
            ctx.strokeStyle= "red";
            ctx.strokeRect(0, y, w, 0.1);
        }
        ctx.strokeStyle = "rgba(0,0,0,0.4)";
        ctx.strokeRect(0, y, w, 0.1);
    }

}
